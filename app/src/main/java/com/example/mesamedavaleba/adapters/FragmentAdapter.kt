package com.example.mesamedavaleba.adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.mesamedavaleba.fragments.FirstFragment
import com.example.mesamedavaleba.fragments.SecondFragment
import com.example.mesamedavaleba.fragments.ThirdFragment

class FragmentAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        if(position == 0) {
            return FirstFragment()
        }

        return SecondFragment()

        return ThirdFragment()

    }
}